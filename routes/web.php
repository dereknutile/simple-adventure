<?php

/*
|--------------------------------------------------------------------------
| Single page controller app's only route
|--------------------------------------------------------------------------
*/

Route::get('/{any}', 'SinglePageController@index')->where('any', '.*');
